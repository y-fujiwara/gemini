defmodule GeminiWeb.PageController do 
  use GeminiWeb, :controller
  plug :authenticate_user when action in [:index, :show, :edit, :update, :delete]

  def index(conn, _params) do
    render conn, "index.html"
  end
end
