defmodule GeminiWeb.SessionController do 
  use GeminiWeb, :controller

  def new(conn, _) do 
    render conn, "new.html" 
  end

  def create(conn, %{"session" => %{"username" => user, "password" => pass}}) do
    case GeminiWeb.Auth.login_by_username_add_pass(conn, user, pass) do 
      {:ok, conn} -> 
        conn 
        # |> put_flash(:info, "Welcome back!") 
        |> redirect(to: page_path(conn, :index)) 
      {:error, _reason, conn} -> 
        conn 
        # |> put_flash(:error, "Invalid username/password combination") 
        |> render("new.html") 
    end 
  end

  def delete(conn, _) do
    conn
    |> GeminiWeb.Auth.logout()
    |> redirect(to: session_path(conn, :new))
  end
end