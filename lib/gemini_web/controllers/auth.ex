defmodule GeminiWeb.Auth do
    import Phoenix.Controller
    alias GeminiWeb.Router.Helpers
    alias Gemini.Accounts.User
    import Plug.Conn
    import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

    @doc """
    pipeline実行時の初期化関数
    """
    def init(opts) do
      # キーワードリストから:repoの箇所の値を取得する
      # 無ければexception(つまりは必須)
      Keyword.fetch!(opts, :context)
    end
  
    @doc """
    pipelineの一部として実行される関数
    """
    def call(conn, context) do
      user_id = get_session(conn, :user_id)
      cond do
        user = conn.assigns[:current_user] -> put_current_user(conn, user)
        user = user_id && context.get_user(user_id) ->
          # assignでconnを変更する(importされた関数)
          # これによって:current_userがコントローラやビューで使えるようになる
          # assign(conn, :current_user, user)
          put_current_user(conn, user)
        true ->
          assign(conn, :current_user, nil)
      end
    end
  
    def login(conn, user) do
      conn
      |> put_current_user(user)
      |> put_session(:user_id, user.id)
      |> configure_session(renew: true) # セッションキーとかを新しくしている(セキュリティのため)
    end
  
    def logout(conn) do
      configure_session(conn, drop: true)
    end
  
    def login_by_username_add_pass(conn, username, given_pass) do
      user = Gemini.Accounts.login_user(username)
  
      # 複数の値で分岐しているためcaseではなくcond(caseは与えられた1つの値に対する分岐)
      cond do
        user && checkpw(given_pass, user.password_hash) ->
          {:ok, login(conn, user)}
        user ->
          # 認証失敗時
          {:error, :unauthorized, conn}
        true ->
          # 列挙攻撃を防ぐためのダミー処理
          # ここで強制falseにしないとパスワードが予測可能になる
          dummy_checkpw()
          {:error, :not_found, conn}
      end
    end

    @doc """
    認証用関数の共通化
    """
    def authenticate_user(conn, _opts) do
      # Plugで追加したassignの呼び出しが可能かどうか
      if conn.assigns.current_user do
        conn
      else
        conn
        # |> put_flash(:error, "You must be logged in to access that page")
        |> redirect(to: Helpers.session_path(conn, :new))
        |> halt()
      end
    end
  
    defp put_current_user(conn, user) do
      # 第二引数はsalt
      token = Phoenix.Token.sign(conn, "user socket", user.id)
  
      conn
      |> assign(:current_user, user)
      |> assign(:user_token, token) # トークンを突っ込んでapp.html.eexより使う
    end
  end