defmodule GeminiWeb.Router do
  use GeminiWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug GeminiWeb.Auth, context: Gemini.Accounts # 追加 
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", GeminiWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get    "/login",  SessionController, :new
    post   "/login",  SessionController, :create
    delete "/logout", SessionController, :delete
    resources "/users", UserController
  end

  # Other scopes may use custom stacks.
  # scope "/api", GeminiWeb do
  #   pipe_through :api
  # end
end
