defmodule Gemini.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Gemini.Accounts.User


  schema "users" do
    field :avatar, :string
    field :email, :string
    field :showname, :string
    field :username, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :is_auth, :boolean
    field :is_delete, :boolean

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:username, :showname, :password, :email, :avatar, :is_auth, :is_delete])
    |> validate_required([:username, :showname, :password, :email]) # 必須項目はこれだけ
    |> validate_format(:email, ~r/@/) # emailには@が必須
    |> unique_constraint(:username) # ユニーク制約
    |> unique_constraint(:email)
    |> validate_length(:password, min: 6) # パスワードは6文字以上
    |> put_pass_hash()
  end


  # パスワードハッシュ化用関数
  defp put_pass_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(pass))
      _ ->
        changeset
    end
  end
end
