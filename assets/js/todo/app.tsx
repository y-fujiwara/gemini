import * as Models from './models';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
// import AppDispatcher from './dispatcher';
import AppActions from './actions';
import Store from './store';
import * as FluxUtils from 'flux/utils';

/**
 * TodoForm用Props
 * 
 * @interface TodoFormProps
 * @extends {React.Props<{}>}
 */
interface TodoFormProps extends React.Props<{}> {
    /**
     * Submitイベント用
     * 
     * @memberof TodoFormProps
     */
    onSubmit: (text: string) => void;
}

/**
 * Form用State
 * 
 * @interface TodoFormState
 */
interface TodoFormState {
    /**
     * Formへの入力文字列
     * 
     * @type {string}
     * @memberof TodoFormState
     */
    inputText: string;
}

/**
 * Todoリストレンダリングクラス
 * 
 * @class TodoForm
 * @extends {React.Component<{}, {}>}
 */
class TodoForm extends React.Component<TodoFormProps, TodoFormState> {

    constructor(props: TodoFormProps) {
        super(props);
        this.state = {inputText: ''};
    }

    /**
     * FormのSubmitイベントハンドラ
     * 
     * @private
     * @param {React.SyntheticEvent<Event>} e 
     * @memberof TodoForm
     */
    private handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        this.props.onSubmit(this.state.inputText);
        this.setState({inputText: ''} as TodoFormState);
    }

    /**
     * 
     * 
     * @private
     * @param {React.FormEvent<HTMLInputElement>} e 
     * @memberof TodoForm
     */
    private handleTodoInputChange(e: React.FormEvent<HTMLInputElement>) {
        let inputValue = e.currentTarget.value;
        this.setState({inputText: inputValue} as TodoFormState);

    }

    /**
     * Override
     * レンダリング実行関数
     * 
     * @returns 
     * @memberof TodoForm
     */
    public render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <label htmlFor='todoInput'>TODO:</label>
                    <input id='todoInput' type='text' 
                      value={this.state.inputText} onChange={this.handleTodoInputChange.bind(this)} />
                    <input type='submit' value='追加' />
                </form>
            </div>);
    }
}

/**
 * 
 * 
 * @interface TodoListProps
 * @extends {React.Props<{}>}
 */
interface TodoListProps extends React.Props<{}> {
    /**
     * 
     * 
     * @type {Models.TodoItem[]}
     * @memberof TodoListProps
     */
    todoItems: Models.TodoItem[];

    /**
     * 削除イベント
     * 
     * @memberof TodoListProps
     */
    onTodoItemRemove: (id: number) => void;
}

/**
 * TodoListレンダリングクラス
 * 
 * @class TodoList
 * @extends {React.Component<{}, {}>}
 */
class TodoList extends React.Component<TodoListProps, {}> {
    /**
     * Override
     * レンダリング実行関数
     * 
     * @returns 
     * @memberof TodoList
     */
    public render() {
        let listItems = this.props.todoItems.map(x => {
            return <li key={x.id}>
                <span>{x.text}</span>
                <button onClick={() => {
                    this.props.onTodoItemRemove(x.id)}} >
                    削除
                </button>
            </li>;
        });
        return <div><ul>{listItems}</ul></div>;
    }
}

/**
 * 
 * 
 * @interface AppState
 */
interface AppState {
    items: Models.TodoItem[];
}

/**
 * Appコンポーネントクラス
 * 
 * @class App
 * @extends {React.Component<{}, {}>}
 */
class App extends React.Component<{}, AppState> {

    constructor(props: {}) {
        super(props);
        this.state = {items: [] as Models.TodoItem[]};
    }

    static getStores() {
        return [Store];
    }

    static calculateState() {
        return {
            items: Store.getState()
        } as AppState;
    }

    /**
     * TodoFormのPropsに渡すSubmitイベント
     * 
     * @private
     * @memberof App
     */
    private handleTodoFormSubmit(text: string) {
        AppActions.addTodo(text);
    }

    private handleTodoListTodoItemRemove(id: number) {
        AppActions.removeTodo(id);
    }

    render() {
        return (
            <div>
                <h1>ToDoリスト</h1>
                <hr />
                <TodoForm onSubmit={this.handleTodoFormSubmit.bind(this)} />
                <TodoList todoItems={this.state.items} 
                    onTodoItemRemove={this.handleTodoListTodoItemRemove.bind(this) } />
            </div>);
    }
}

const AppContainer = FluxUtils.Container.create(App);

ReactDOM.render(<AppContainer />, document.getElementById("todo"));
