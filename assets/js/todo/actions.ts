import * as Models from './models';
import AppDispatcher from './dispatcher';

/**
 * Actionクラス
 * Ajaxアクセスとかもここでやるらしい
 * 
 * @class AppAction
 */
class AppActions {
    public addTodo(text: string) {
        AppDispatcher.dispatch(new Models.AddAction(text));
    }

    public removeTodo(id: number) {
        AppDispatcher.dispatch(new Models.RemoveAction(id));
    }
}
export default new AppActions();