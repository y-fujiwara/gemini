import * as Models from './models';
import AppDispatcher from './dispatcher';

// TodoListクラスをシングルトンとして提供する
export default new Models.TodoList(AppDispatcher);