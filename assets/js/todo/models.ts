import * as Flux from 'flux';
import * as FluxUtils from 'flux/utils';

/**
 * 
 * 
 * @export
 * @class TodoItem
 */
export class TodoItem {
    /**
     * タスクID
     * @private
     * @type {number}
     * @memberof TodoItem
     */
    public id: number = Date.now();

    /**
     * Creates an instance of TodoItem.
     * @param {string} text 
     * @memberof TodoItem
     */
    constructor(public text: string) {
        // pass
    }
}

/**
 * Todoリスト追加アクション
 * 
 * @export
 * @class AddAction
 */
export class AddAction {
    /**
     * Creates an instance of AddAction.
     * @param {string} text 
     * @memberof AddAction
     */
    constructor(public text: string) {}
}

/**
 * Todoリスト削除アクション
 * 
 * @export
 * @class RemoveAction
 */
export class RemoveAction {
    /**
     * Creates an instance of RemoveAction.
     * @param {number} id 
     * @memberof RemoveAction
     */
    constructor(public id: number) {}
}

/**
 * StoreのジェネリクスはTPayloadなのでコンストラクタのDispatcherのジェネリクス型と同じ
 * __onDispatchメソッドの引数型なども同様
 * 
 * @export
 * @class TodoList
 */
export class TodoList extends FluxUtils.ReduceStore<TodoItem[], any> {
    /**
     * Creates an instance of TodoList.
     * @param {Flux.Dispatcher<any>} d 
     * @memberof TodoList
     */
    constructor(d: Flux.Dispatcher<any>) {
        super(d);
    }

    getInitialState() {
        return [] as TodoItem[];
    }

    reduce(state: TodoItem[], action: any) {
        if (action instanceof AddAction) {
            let text = (action as AddAction).text; 
            let copy = state.slice();
            copy.push(new TodoItem(text));
            return copy;
        }
        if (action instanceof RemoveAction) {
            let id = (action as RemoveAction).id; 
            return state.filter(x => x.id != id);
        }
        return state;
    }
}