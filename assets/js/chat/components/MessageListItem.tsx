import * as React from 'react';
import Message from './../models/Message';
import { Comment } from 'semantic-ui-react'

interface MessageListItemProps extends React.Props<{}> {
    message: Message;
}

export default class MessageListItem extends React.Component<MessageListItemProps, {}> {
    public render() {
        let message = this.props.message;
        let src = 'https://www.gravatar.com/avatar/' + message.authorName + '?d=identicon';
        return ( 
          <Comment className='message-list-item'>
            <Comment.Avatar src={src}/>
            <Comment.Content>
              <Comment.Author as='h5' className='message-author-name'>
                  {message.authorName}
              </Comment.Author>
              <Comment.Metadata className='message-time'>
                <div>{message.date.toLocaleString()}</div>
              </Comment.Metadata>
              <Comment.Text className='message-text'>{message.text}</Comment.Text>
              <Comment.Actions>
                <Comment.Action>Reply</Comment.Action>
              </Comment.Actions>
            </Comment.Content>
          </Comment>
        );
    }
}
