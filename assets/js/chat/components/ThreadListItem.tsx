import * as React from 'react';
import ChatThreadActionCreators from './../actions/ChatThreadActionCreators';
import Thread from './../models/Thread';
import { Header, Menu } from 'semantic-ui-react'

const styles: React.CSSProperties = {
    text: {
        textOverflow: "ellipsis",
        overflow: "hidden",
        whiteSpace: "nowrap"
    }
}

interface ThreadListItemProps extends React.Props<{}> {
    thread: Thread;
    currentThreadID: string;
}

export default class ThreadListItem extends React.Component<ThreadListItemProps, {}> {
    private onClick() {
        ChatThreadActionCreators.clickThread(this.props.thread.id);
    }

    private listClassName() {
        // let className = 'thread-list-item';
        if (this.props.thread.id === this.props.currentThreadID) {
            return true;
        }
        return false;
    }

    public render() {
        let thread = this.props.thread;
        let lastMessage = thread.lastMessage;
        return (
            <Menu.Item
                name={thread.name}
                active={this.listClassName()}
                onClick={this.onClick.bind(this)} >
                <Header color="grey" as='h4'>{thread.name}</Header>
                <p style={styles.text}>{lastMessage.text}</p> 
            </Menu.Item>
        );
    }
}

