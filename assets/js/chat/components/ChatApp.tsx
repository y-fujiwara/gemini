import * as React from 'react';
import MessageContainer from './MessageSection';
import ThreadContainer from './ThreadSection';

export default class ChatApp extends React.Component<{}, {}> {
    render() {
        return (
            <div className='chatapp'>
                <ThreadContainer />
                <MessageContainer /> 
            </div>
        );
    }
}