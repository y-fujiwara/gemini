import * as React from 'react';
import MessageComposer from './MessageComposer';
import MessageListItem from './MessageListItem';
import MessageStore from './../stores/MessageStore';
import ThreadStore from './../stores/ThreadStore';
import Message from './../models/Message';
import Thread from './../models/Thread';
import * as FluxUtils from 'flux/utils';
import { Comment } from 'semantic-ui-react'

interface MessageSectionState {
    messages: Message[];
    thread: Thread;
}

class MessageSection extends React.Component<{}, MessageSectionState> {
    constructor(props: {}) {
        super(props);
        this.state = MessageSection.calculateState();
    } 
    
    static getStores() {
        return [MessageStore, ThreadStore];
    }

    static calculateState(): MessageSectionState {
        return {
            messages: MessageStore.getAllForCurrentThread(),
            thread: ThreadStore.getCurrent()
        } as MessageSectionState;
    }

    private getMessageListItem(message: Message) {
        return (
            <MessageListItem
                key={message.id}
                message={message} />
        );
    }

    private scrollToBottom() {
        let ul = document.getElementById("message-list") as Element;// ReactDOM.findDOMNode(this.refs['messageList']);
        ul.scrollTop = ul.scrollHeight;
    }

    public componentDidUpdate() {
        this.scrollToBottom();
    }

    public render() {
        let messageListItems = this.state.messages.map(this.getMessageListItem);
        return (
            <div className='message-section'>
                <h3 className='message-thread-heading'>
                    {this.state.thread.name}
                </h3>
                <Comment.Group className='message-list' id="message-list">
                    {messageListItems}
                </Comment.Group>
                <MessageComposer threadID={this.state.thread.id} />
            </div>
        );
    }
}

export default FluxUtils.Container.create(MessageSection);