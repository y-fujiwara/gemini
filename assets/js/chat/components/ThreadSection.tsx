import * as React from 'react';
import ThreadListItem from './ThreadListItem';
import ThreadStore from './../stores/ThreadStore';
import UnreadThreadStore from './../stores/UnreadThreadStore';
import Thread from './../models/Thread';
import * as FluxUtils from 'flux/utils';
import { Label, Divider, Sidebar, Menu } from 'semantic-ui-react'


interface ThreadSectionState {
    threads: Thread[];
    currentThreadID: string;
    unreadCount: number;
}

class ThreadSection extends React.Component<{}, ThreadSectionState> {
    constructor(props: {}) {
        super(props);
        this.state = ThreadSection.calculateState();
    } 
    
    static getStores() {
        return [ThreadStore, UnreadThreadStore];
    }

    static calculateState(): ThreadSectionState {
        return {
            threads: ThreadStore.getAllChrono(),
            currentThreadID: ThreadStore.getCurrentID(),
            unreadCount: UnreadThreadStore.getCount()
        } as ThreadSectionState;
    }

    render () {
        let threadListItems = this.state.threads.map(thread => {
            return (
                <ThreadListItem
                    key={thread.id}
                    thread={thread}
                    currentThreadID={this.state.currentThreadID} />
            );
        });
        let unread =
        <Label as='span'>Unread thread<Label.Detail>
            {this.state.unreadCount}
        </Label.Detail></Label>;
        
        return (
            <Sidebar as={Menu} visible={true} animation='push' width='thin' icon='labeled' vertical inverted>
                <Divider inverted horizontal>Unread Count</Divider>
                {unread}
                <Divider inverted horizontal>Threads</Divider>
                {threadListItems}
            </Sidebar>
        );
    }
}
export default FluxUtils.Container.create(ThreadSection);