import * as React from 'react';
import ChatMessageActionCreators from './../actions/ChatMessageActionCreators';
import { Form, TextArea } from 'semantic-ui-react'

const ENTER_KEY_CODE = 13;

interface MessageComposerProps extends React.Props<{}> {
    threadID: string;
}

interface MessageComposerState {
    text: string;
}

export default class MessageComposer extends React.Component<MessageComposerProps, MessageComposerState> {
    constructor(props: MessageComposerProps) {
        super(props);
        this.state = {text : ''};
    }

    private onChange(event: React.SyntheticEvent<HTMLInputElement>) {
        this.setState({ text: event.currentTarget.value});
    }

    private onKeyDown(event: React.KeyboardEvent<HTMLInputElement>) {
        if(event.keyCode === ENTER_KEY_CODE) {
            event.preventDefault();
            let text = this.state.text.trim();
            if (text) {
                ChatMessageActionCreators.createMessage(text, this.props.threadID);
                this.setState({ text: ''});
            }
        }
    }

    public render() {
        return ( 
            <Form size="large">
                <TextArea
                    autoHeight
                    rows={1}
                    className='message-composer' 
                    placeholder='Input Message'
                    name='message'
                    value={this.state.text}
                    onChange={this.onChange.bind(this)}
                    onKeyDown={this.onKeyDown.bind(this)} />
            </Form>
        );
    }
}
