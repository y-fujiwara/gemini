import ChatAppDispatcher from './../dispatcher/ChatAppDispatcher';
import MessageStore from './MessageStore';
import ThreadStore from './ThreadStore';
import ClickThread from './../messages/ClickThread';
import ReceiveRawMessages from './../messages/ReceiveRawMessages';
import Thread from './../models/Thread';
import * as FluxUtils from 'flux/utils';
import {Map} from 'immutable';

class UnreadThreadStore extends FluxUtils.ReduceStore<number, any> {
    getInitialState() {
        return 0;
    }

    public getCount() {
        let threads = ThreadStore.getAll() as Map<string, Thread>;
        let unreadCount = 0;
        threads.keySeq().forEach((id: string) => {
            if (!threads.get(id).lastMessage.isRead) {
                unreadCount++;
            }
        });
        return unreadCount;
    }

    public reduce(state: number, action: any) {
        ChatAppDispatcher.waitFor([
            ThreadStore.getDispatchToken(),
            MessageStore.getDispatchToken()
        ]);
        if (action instanceof ClickThread) {
            return this.getCount();
        }
        if (action instanceof ReceiveRawMessages) {
            return this.getCount();
        }
        return state;
    }
}

export default new UnreadThreadStore(ChatAppDispatcher);