import ChatAppDispatcher from './../dispatcher/ChatAppDispatcher';
import ChatMessageUtils from './../utils/ChatMessageUtils';
import ThreadStore from './ThreadStore';
import ClickThread from './../messages/ClickThread';
import CreateMessage from './../messages/CreateMessage';
import ReceiveRawMessages from './../messages/ReceiveRawMessages';
import Message from './../models/Message';
import RawMessage from './../models/RawMessage';
import * as FluxUtils from 'flux/utils';
import * as Immutable from 'immutable';
// import * as objectAssign from 'object-assign';

/*
interface StoreMessage {
    [key: string]: Message
};
*/

type StoreMessage = Immutable.Map<string, Message>;

class MessageStore extends FluxUtils.ReduceStore<StoreMessage, any> {
    getInitialState() {
        return Immutable.Map({}) as StoreMessage;
    }

    public get(id: string) {
        return this.getState().get(id);
    }

    public getAll() {
        return this.getState();
    }

    public getAllForThread(threadID: string) {
        let threadMessages = new Array<Message>();
        this.getState().keySeq().forEach((id: string) => {
            if (this.getState().get(id).threadID === threadID) {
                threadMessages.push(this.getState().get(id));
            }
        });
        threadMessages.sort((a, b) => {
            if (a.date < b.date) {
                return -1;
            } else if ( a.date > b.date) {
                return 1;
            }
            return 0;
        });
        return threadMessages;
    }

    public getAllForCurrentThread() {
        return this.getAllForThread(ThreadStore.getCurrentID());
    }

    public reduce(state: StoreMessage, action: any) {
        if (action instanceof ClickThread) {
            // ThreadStoreの処理を待つ
            ChatAppDispatcher.waitFor(
                [ThreadStore.getDispatchToken()]);
            this.markAllInThreadRead(state, ThreadStore.getCurrentID());
            return state;
        }
        if (action instanceof CreateMessage) {
            let message = ChatMessageUtils.getCreatedMessageData(
                (action as CreateMessage).text,
                ThreadStore.getCurrentID());
            let newState = state.set(message.id, message);
            return newState;
        }
        if (action instanceof ReceiveRawMessages) {
            let ret = this.addMessages(
                state,
                (action as ReceiveRawMessages).rawMessages);
            ChatAppDispatcher.waitFor(
                [ThreadStore.getDispatchToken()]);
            this.markAllInThreadRead(ret, ThreadStore.getCurrentID());
            return ret;
        }
        return state;
    }

    /**
     * 
     * 
     * @private
     * @param {RawMessage[]} rawMessages 
     * @memberof MessageStore
     */
    private addMessages(state: StoreMessage, rawMessages: RawMessage[]): StoreMessage {
        let ret = state;
        rawMessages.forEach(message => {
            if (!state.get(message.id)) {
                let tmpMap = Immutable.Map<string, Message>([[
                    message.id, ChatMessageUtils.convertRawMessage(
                    message,
                    ThreadStore.getCurrentID())]]);
                ret = ret.merge(tmpMap);
            }
        });
        return ret;
    }

    /**
     * 
     * 
     * @private
     * @param {string} threadID 
     * @memberof MessageStore
     */
    private markAllInThreadRead(state: StoreMessage, threadID: string): StoreMessage {
        let ret = state;
        state.keySeq().forEach((id: string) => {
            if (state.get(id).threadID === threadID) {
                ret = ret.update(id, (mes) => { 
                    return mes.set('isRead', true) as Message; 
                });
            }
        });
        return ret;
    }
}

export default new MessageStore(ChatAppDispatcher);