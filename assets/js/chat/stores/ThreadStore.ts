import * as FluxUtils from 'flux/utils';
import ChatAppDispatcher from './../dispatcher/ChatAppDispatcher';
import ChatMessageUtils from './../utils/ChatMessageUtils';
import ClickThread from './../messages/ClickThread';
import ReceiveRawMessages from './../messages/ReceiveRawMessages';
import RawMessage from './../models/RawMessage';
import Thread from './../models/Thread';
import * as Immutable from 'immutable';
// import * as objectAssign from 'object-assign';

/*
interface ThreadState {
    currentID: string;
    threads: Immutable.Map<string, Thread>;
}
*/

type ThreadMap = Immutable.Map<string, Thread>;
type ThreadState = Immutable.Map<string, string | ThreadMap>;

class ThreadStore extends FluxUtils.ReduceStore<ThreadState, any> {
    getInitialState() {
        return Immutable.Map({
            currentID: undefined, 
            threads: Immutable.Map({}) as ThreadMap
        }) as ThreadState;
    }

    public reduce(state: ThreadState, action: any) {
        let ret = state;
        if (action instanceof ClickThread) {
            let x = action as ClickThread;
            ret = ret.set('currentID', x.threadID) as ThreadState;
            ret = ret.setIn(['threads', ret.get('currentID'), 'lastMessage', 'isRead'], true);
            return ret;
        }
        if (action instanceof ReceiveRawMessages) {
            ret = this.init(state, (action as ReceiveRawMessages).rawMessages);
            return ret;
        }
        return state;
    }

    /**
     * データ取得用Getter
     * 
     * @param {string} id 
     * @returns 
     * @memberof ThreadStore
     */
    public get(id: string) {
        return (this.getState().get('threads') as ThreadMap).get(id);
    }

    /**
     * データ全取得用Getter
     * 
     * @returns 
     * @memberof ThreadStore
     */
    public getAll() {
        let ret = this.getState().get('threads') as ThreadMap;
        return ret;
    }

    /**
     * 現在のスレッドIDを取得するGetter
     * 
     * @returns 
     * @memberof ThreadStore
     */
    public getCurrentID(): string {
        return this.getState().get('currentID') as string;
    }

    /**
     * 現在参照中のスレッドのメッセージ取得用Getter
     * 
     * @returns 
     * @memberof ThreadStore
     */
    public getCurrent() {
        return this.get(this.getCurrentID());
    }
    
    /**
     * 初期化用メソッド
     * 
     * @param {RawMessage[]} rawMessages 
     * @memberof ThreadStore
     */
    private init(state: ThreadState, rawMessages: RawMessage[]): ThreadState {
        // すでに存在するメッセージをスレッドに振り分け
        let ret = state;
        rawMessages.forEach(message => {
            let threadID = message.threadID;
            let thread = (state.get('threads') as ThreadMap).get('threadID');
            if (thread && thread.get('lastMessage').get('date').getTime() > message.timestamp) {
                return;
            }

            let tmpMap = Immutable.Map<string, Thread>([[
                threadID, 
                new Thread( 
                    threadID, 
                    message.threadName, 
                    ChatMessageUtils.convertRawMessage(message, state.get('currentID') as string))
            ]]);
            ret = ret.set('threads', (ret.get('threads') as ThreadMap).merge(tmpMap));
        });
        // スレッドのどれにも参照していないときは最後に追加されているものにする
        if (!state.get('currentID')) {
            let allChrono = this.getAllChronoCopy(ret);
            ret = ret.set('currentID', allChrono[allChrono.length - 1].get('id'));
        }
        ret = ret.setIn(['threads', ret.get('currentID'), 'lastMessage', 'isRead'], true);
        return ret;
    }

    /**
     * 最終投稿日時でソートしたスレッド一覧を取得する
     * 
     * @private
     * @returns 
     * @memberof ThreadStore
     */
    public getAllChrono() {
        let orderedThreads = new Array<Thread>();
        let threads = this.getState().get('threads') as ThreadMap;
        threads.keySeq().forEach((id: string) =>  {
            let thread = threads.get(id);
            orderedThreads.push(thread);
        });

        orderedThreads.sort((a, b) => {
            if (a.get('lastMessage').get('date') < b.get('lastMessage').get('date')) {
                return -1;
            } else if (a.get('lastMessage').get('date') > b.get('lastMessage').get('date')) {
                return 1;
            }
            return 0;
        });
        return orderedThreads;
    }

    /**
     * 最終投稿日時でソートしたスレッド一覧を取得する
     * 
     * @private
     * @returns 
     * @memberof ThreadStore
     */
    private getAllChronoCopy(state: ThreadState) {
        let orderedThreads = new Array<Thread>();
        let threads = state.get('threads') as ThreadMap;
        threads.keySeq().forEach((id: string) => {
            let thread = threads.get(id);
            orderedThreads.push(thread);
        });

        orderedThreads.sort((a, b) => {
            if (a.get('lastMessage').get('date') < b.get('lastMessage').get('date')) {
                return -1;
            } else if (a.get('lastMessage').get('date') > b.get('lastMessage').get('date')) {
                return 1;
            }
            return 0;
        });
        return orderedThreads;
    }
}

export default new ThreadStore(ChatAppDispatcher);