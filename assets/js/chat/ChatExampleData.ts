import RawMessage from './models/RawMessage';

class ChatExampleData {
    public init() {
        localStorage.clear();
        localStorage.setItem('messages', JSON.stringify([
            new RawMessage('m_1', 't_1', 'Jing and Bill', 'Bill', 
                'Hey Jing, want to give a Flux talk at ForwardJS?', Date.now() - 99999), 
            new RawMessage('m_2', 't_1', 'Jinh and Bill', 'Bill', 
                'Seems like a pretty cool conference.', Date.now() - 89999),
            new RawMessage('m_3', 't_1', 'Jing and Bill', 'Bill',
                'Sounds good. Will they be serving dessert?', Date.now() - 79999),
            new RawMessage('m_4', 't_2', 'Dave and Bill', 'Bill',
                'Hey Dave, want to ge a beer after the conference?', Date.now() - 69999),
            new RawMessage('m_5', 't_2', 'Dave and Bill', 'Dave', 
                'Totally!, Meet you at the hotel bar.', Date.now() - 59999),
            new RawMessage('m_6', 't_3', 'Functinal Heads', 'Bill',
                'Hey Brian, are you going to be talking about functional stuff?', Date.now() - 49999),
            new RawMessage('m_7', 't_3', 'Bill and Brian', 'Brian',
                'At ForwardJS? Yeah, of course. See you there!', Date.now() - 39999)
        ]));

    }
}

export default new ChatExampleData();