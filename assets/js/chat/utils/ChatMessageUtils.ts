import Message from './../models/Message';
import RawMessage from './../models/RawMessage';

class ChatMessageUtils {
    public convertRawMessage(
        rawMessage: RawMessage, currentThreadID: string ) {
        return new Message(
            rawMessage.id,
            rawMessage.threadID,
            rawMessage.authorName,
            new Date(rawMessage.timestamp),
            rawMessage.text,
            rawMessage.threadID === currentThreadID);
    }

    public getCreatedMessageData(text: string, currentThreadID: string) {
        let timestamp = Date.now();
        return new Message(
            'm_' + timestamp,
            currentThreadID,
            'Bill',
            new Date(timestamp),
            text,
            true);
    }
}

export default new ChatMessageUtils();