import ChatServerActionCreators from './../actions/ChatServerActionCreators';
import Message from './../models/Message';
import RawMessage from './../models/RawMessage';

class ChatWebAPIUtils {
    public getAllMessages() {
        // simulate retreving data from a database
        let data = localStorage.getItem('messages');
        let rawMessages = JSON.parse(data === null ? '' : data);
        // simulate success callback
        ChatServerActionCreators.receiveAll(rawMessages);
    }

    public createMessage(message: Message, threadName: string) {
        // simulate writing to a database
        let data = localStorage.getItem('messages');
        let rawMessages = JSON.parse(data === null ? '' : data) as RawMessage[];

        let timestamp = Date.now();
        let id = 'm_' + timestamp;
        // threadIDが存在していなければ日付から作る
        let threadID = message.threadID || ('t_' + Date.now());
        let createdMessage = new RawMessage(
            id,
            threadID,
            threadName,
            message.authorName,
            message.text,
            timestamp);

        rawMessages.push(createdMessage);
        localStorage.setItem('messages', JSON.stringify(rawMessages));

        // simulate success callback 
        setTimeout(() => ChatServerActionCreators.receiveCreatedMessage(createdMessage),
        0);
    }
}

export default new ChatWebAPIUtils();