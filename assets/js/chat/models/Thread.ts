import Message from './Message';
import * as Immutable from 'immutable';

const ThreadRecord = Immutable.Record({
    id: '',
    name: '',
    lastMessage: null
});

/**
 * 
 * 
 * @export
 * @class Thread
 */
export default class Thread extends ThreadRecord {
    readonly id: string;
    readonly name: string;
    readonly lastMessage: Message;
    
    /**
     * Creates an instance of Thread.
     * @param {string} id 
     * @param {string} name 
     * @param {Message} lastMessage 
     * @memberof Thread
     */
    constructor(id: string, name: string, lastMessage: Message) {
        super({
            id: id,
            name: name,
            lastMessage: lastMessage
        });
     }
}