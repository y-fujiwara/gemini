import * as Immutable from 'immutable';

const MessageRecord = Immutable.Record({
    id: '',
    threadID: '',
    authorName: '',
    date: null,
    text: '',
    isRead: false
});

export default class Message extends MessageRecord {

    readonly id: string;
    readonly threadID: string;
    readonly authorName: string;
    readonly date: Date;
    readonly text: string;
    readonly isRead: boolean;

    constructor(id: string, threadID: string, authorName: string, date: Date, text: string, isRead: boolean) {
        super({
            id: id,
            threadID: threadID,
            authorName: authorName,
            date: date,
            text: text,
            isRead: isRead
        });
    }
}