import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ChatApp from './components/ChatApp';
import ChatExampleData from './ChatExampleData';
import ChatWebAPIUtils from './utils/ChatWebAPIUtils';
// import * as Phoenix from 'phoenix';
// import 'semantic-ui-css/semantic.min.css';


ChatExampleData.init();
ChatWebAPIUtils.getAllMessages();

let Render = () => {
    ReactDOM.render( <ChatApp />, document.getElementById('chat'));
};

export default Render;