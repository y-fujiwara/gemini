import ChatAppDispatcher from './../dispatcher/ChatAppDispatcher';
import ClickThread from './../messages/ClickThread';

class ChatThreadActionCreators {
    public clickThread(threadID: string) {
        ChatAppDispatcher.dispatch(new ClickThread(threadID));
    }
}

export default new ChatThreadActionCreators();