import ChatAppDispatcher from './../dispatcher/ChatAppDispatcher';
import CreateMessage from './../messages/CreateMessage';
import ChatWebAPIUtil from './../utils/ChatWebAPIUtils';
import ChatMessageUtils from './../utils/ChatMessageUtils';

class ChatMessageActionCreators {
    public createMessage(text: string, currentThreadID: string) {
        ChatAppDispatcher.dispatch(
            new CreateMessage(text, currentThreadID));
        let message = ChatMessageUtils.getCreatedMessageData(text, currentThreadID);
        ChatWebAPIUtil.createMessage(message, '');
    }
}

export default new ChatMessageActionCreators();