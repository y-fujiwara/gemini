# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Gemini.Repo.insert!(%Gemini.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
# !マーク付きの関数を呼ぶとおかしいときに例外発行してくれる
# 通常の関数は戻り値のタプルで判断する
Gemini.Repo.insert! %Gemini.Accounts.User{ 
  id: 1,
  username: "administrator",
  showname: "admin",
  email: "admin@example.com",
  is_auth: true,
  password_hash: Comeonin.Bcrypt.hashpwsalt("administrator")
}