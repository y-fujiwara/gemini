defmodule Gemini.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :string, null: false, unique: true
      add :showname, :string
      add :email, :string, null: false, unique: true
      add :avatar, :string
      add :is_delete, :boolean, default: false
      add :is_auth, :boolean, default: false

      timestamps()
    end

    create unique_index(:users, [:username])
    create unique_index(:users, [:email])
  end
end
