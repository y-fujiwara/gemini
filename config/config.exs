# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :gemini,
  ecto_repos: [Gemini.Repo]

# Configures the endpoint
config :gemini, GeminiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "lVgoS5wZYzl/emz8G5J9MsIeqHnqCh2MNXmvSy9W5XlHqless6aRBweAld//8iYT",
  render_errors: [view: GeminiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Gemini.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
